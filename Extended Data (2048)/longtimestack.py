import numpy as np
from numpy import ubyte
from PIL import Image
import cv2

# manresa_scalar_512

X_RES = 1280
Y_RES = 720
NUM_PIXEL_VALS = 3
NUM_FRAMES = 2048
FILE_NAME2 = "seabright_scalar_2048.raw"

def generate_seabright_timestack(arr):
    slice_loc = 639
    line_start = (slice_loc, 0)
    line_end = (slice_loc, 719)
    slice_thickness = 1
    img_arr = np.empty((Y_RES, X_RES, NUM_PIXEL_VALS), dtype=ubyte)
    for col in range(X_RES):
        for row in range(Y_RES):
            for val in range(NUM_PIXEL_VALS):
                img_arr[row, col, 2 - val] = arr[0, row, col, val]
    img = cv2.line(img_arr, line_start, line_end, (255, 0, 0), slice_thickness)
    img_slice = Image.fromarray(img, 'RGB')
    img_slice.save("seabright_slice", format="PNG")
    seabright_timestack_arr = np.empty((Y_RES, NUM_FRAMES, NUM_PIXEL_VALS), dtype=ubyte)
    for frame in range(NUM_FRAMES):
        for row in range(Y_RES):
            for val in range(NUM_PIXEL_VALS):
                seabright_timestack_arr[row, frame, 2-val] = arr[frame, row, slice_loc, val]

    stack = Image.fromarray(seabright_timestack_arr, 'RGB')
    stack.save("seabright_stack", format="PNG")
    stack.show()


def main():

    seabright_file = open(FILE_NAME2, "rb")
    seabright_arr = np.fromfile(FILE_NAME2, dtype=ubyte)
    seabright_file.close()

    seabright_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_PIXEL_VALS)

    generate_seabright_timestack(seabright_arr)


main()