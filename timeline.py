import numpy as np
from numpy import int_
from PIL import Image
import math
import cv2
import numpy as np
from numpy import ubyte
from PIL import Image

X_RES = 1280
Y_RES = 720
NUM_PIXEL_VALS = 3
NUM_VEC_VALS = 2
NUM_FRAMES = 512
SLICE_LOC = 400
FPS = 30
SQUARE_SIZE = 5
MAX_DIST = 630
NUM_BUOYS = 26
BUOY_SPACING = 50
MANRESA_SCALAR_FILE_NAME = "manresa_scalar_512.raw"
MANRESA_VECTOR_FILE_NAME = "manresa_vector_512.raw"

SEABRIGHT_SCALAR_FILE_NAME = "seabright_scalar.raw"
SEABRIGHT_VECTOR_FILE_NAME = "seabright_vector.raw"

SEABRIGHT_SWASH_MAX = 600
SEABRIGHT_SWASH_MIN = 200

MANRESA_LEFT_OFFSET = 300
MANRESA_RIGHT_OFFSET = 100

# Black-Red Color Scale
def convert_to_rgb(maximum, value):
    if value > maximum:
        print(value)
    return 0, 0, (value / maximum * 255)

# Draw buoy as a 5px x 5px box centered at given buoy location
def draw_buoy(arr, f, r, c, d):
    this_row = r - 2
    this_col = c - 2
    if this_row < 0 or this_col < 0 or this_row + 5 > 720 or this_col + 5 > 1280:
        return
    else:
        for dr in range(SQUARE_SIZE):
            for dc in range(SQUARE_SIZE):
                arr[f, this_row + dr, this_col + dc] = convert_to_rgb(MAX_DIST, d)

# euler integration for updating buoys locations + buoy displacement calculation
def euler(buoy_arr, vec_arr, frame, col):
    # calculate new location and total displacement
    dx = vec_arr[frame, buoy_arr[col][1], buoy_arr[col][0], 0]
    dy = vec_arr[frame, buoy_arr[col][1], buoy_arr[col][0], 1]
    # update buoy location + edge cases
    currX = buoy_arr[col, 0] + dx
    currY = buoy_arr[col, 1] + dy
    if 1279 > currX > -1 and SEABRIGHT_SWASH_MIN < currY < SEABRIGHT_SWASH_MAX:
        buoy_arr[col, 0] += dx
        buoy_arr[col, 1] += dy
        disp = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
        # update buoy displacement
        buoy_arr[col, 2] += disp

# generate seabright timeline from 3D scalar and 3D vector array
def generate_seabright_timeline(seabright_scalar_arr, seabright_vector_array):
    # create array of initial buoy locations and displacements along slice
    buoy_arr = np.empty((X_RES, 3), dtype=int_)
    for col in range(X_RES):
        buoy_arr[col, 0] = col
        buoy_arr[col, 1] = SLICE_LOC
        buoy_arr[col, 2] = 0

    # Video writer settings
    seabright_fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    seabright_video = cv2.VideoWriter('seabright_timeline.mp4', seabright_fourcc, float(FPS), (X_RES, Y_RES))

    # Draw original buoy location line for all frames (background)
    for frame in range(NUM_FRAMES):
        for col in range(X_RES):
            seabright_scalar_arr[frame] = cv2.line(seabright_scalar_arr[frame], (0, SLICE_LOC), (1279, SLICE_LOC), (255, 0, 0), 1)
            # Draw swash zone lines for all frames
            # seabright_scalar_arr[frame] = cv2.line(seabright_scalar_arr[frame], (0, SEABRIGHT_SWASH_MIN), (1279, SEABRIGHT_SWASH_MIN),
            #                                        (0, 255, 0), 1)
            # seabright_scalar_arr[frame] = cv2.line(seabright_scalar_arr[frame], (0, SEABRIGHT_SWASH_MAX),
            #                                        (1279, SEABRIGHT_SWASH_MAX),
            #                                        (0, 255, 0), 1)
    # Draw timeline
    for frame in range(NUM_FRAMES):
        # initial image array
        img = seabright_scalar_arr[frame]
        # draw each buoy and update total displacement/current location
        for col in range(NUM_BUOYS):
            buoy = col * BUOY_SPACING
            draw_buoy(seabright_scalar_arr, frame, buoy_arr[buoy, 1], buoy_arr[buoy, 0], buoy_arr[buoy, 2])
            euler(buoy_arr, seabright_vector_array, frame, buoy)
            # Draw lines between buoys
            # Line color is calculated by taking the average of two connected buoys
            if col > 1:
                img = cv2.line(img, (buoy_arr[buoy - BUOY_SPACING, 0], buoy_arr[buoy - BUOY_SPACING, 1]),
                               (buoy_arr[buoy, 0], buoy_arr[buoy, 1]),
                               convert_to_rgb(MAX_DIST,
                                              ((buoy_arr[buoy - BUOY_SPACING, 2]) +
                                              (buoy_arr[buoy, 2])) / 2), 2)
        seabright_video.write(img)
        # Save first and last frames of timeline
        # if frame == 0:
        #     img = img[:, :, ::-1]
        #     first_slice = Image.fromarray(img, 'RGB')
        #     first_slice.save("seabright_first_slice", format="PNG")
        # if frame == NUM_FRAMES-1:
        #     img = img[:, :, ::-1]
        #     last_slice = Image.fromarray(img, 'RGB')
        #     last_slice.save("seabright_last_slice", format="PNG")
    seabright_video.release()


def generate_manresa_timeline(manresa_scalar_arr, manresa_vector_array, ):

    # Video writer settings
    manresa_fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    manresa_video = cv2.VideoWriter('manresa_timeline.mp4', manresa_fourcc, float(FPS), (X_RES, Y_RES))

    # Draw original buoy location line for all frames (background)
    line_start = (0, 659)
    line_end = (1279, 285)
    for frame in range(NUM_FRAMES):
        manresa_scalar_arr[frame] = cv2.line(manresa_scalar_arr[frame], line_start, line_end, (255, 0, 0), 1)
        # draw swash zone lines for all frames
        # manresa_scalar_arr[frame] = cv2.line(manresa_scalar_arr[frame],
        #                                      (line_start[0], line_start[1] + MANRESA_LEFT_OFFSET),
        #                                      (line_end[0], line_end[1] + MANRESA_RIGHT_OFFSET),
        #                                      (0, 255, 0), 1)
        # manresa_scalar_arr[frame] = cv2.line(manresa_scalar_arr[frame],
        #                                      (line_start[0], line_start[1] - MANRESA_LEFT_OFFSET),
        #                                      (line_end[0], line_end[1] - MANRESA_RIGHT_OFFSET),
        #                                      (0, 255, 0), 1)

    # create array of initial buoy locations and displacements along slice
    buoy_arr2 = np.empty((X_RES, 3), dtype=int_)
    count = 0
    for row in range(Y_RES):
        for col in range(X_RES):
            if manresa_scalar_arr[0, row, col, 0] == 255 \
                    and manresa_scalar_arr[0, row, col, 1] == 0 \
                    and manresa_scalar_arr[0, row, col, 2] == 0:
                buoy_arr2[count, 0] = col
                buoy_arr2[count, 1] = row
                buoy_arr2[count, 2] = 0
                count += 1

    # Draw timeline
    for frame in range(NUM_FRAMES):
        # initial image array
        img = manresa_scalar_arr[frame]
        # draw each buoy and update total displacement/current location
        for col in range(1, NUM_BUOYS):
            buoy = col * BUOY_SPACING
            draw_buoy(manresa_scalar_arr, frame, buoy_arr2[buoy][1], buoy_arr2[buoy][0], buoy_arr2[buoy][2])
            euler(buoy_arr2, manresa_vector_array, frame, buoy)
            # Draw lines between buoys
            # Line color is calculated by taking the average of two connected buoys
            if col > 1:
                img = cv2.line(img, (buoy_arr2[buoy - BUOY_SPACING, 0], buoy_arr2[buoy - BUOY_SPACING, 1]),
                               (buoy_arr2[buoy, 0], buoy_arr2[buoy, 1]),
                               convert_to_rgb(MAX_DIST, buoy_arr2[buoy - BUOY_SPACING, 2] + buoy_arr2[buoy, 2] / 2), 2)
        manresa_video.write(img)
        # Save first and last frames of timeline
        # if frame == 0:
        #     img = img[:, :, ::-1]
        #     first_slice = Image.fromarray(img, 'RGB')
        #     first_slice.save("manresa_first_slice", format="PNG")
        # if frame == NUM_FRAMES-1:
        #     img = img[:, :, ::-1]
        #     last_slice = Image.fromarray(img, 'RGB')
        #     last_slice.save("manresa_last_slice", format="PNG")
    manresa_video.release()


def main():
    # read in scalar seabright file into array
    seabright_scalar_file = open(SEABRIGHT_SCALAR_FILE_NAME, "rb")
    seabright_scalar_arr = np.fromfile(SEABRIGHT_SCALAR_FILE_NAME, dtype=ubyte)
    seabright_scalar_file.close()

    # reshape array to 3D scalar volume
    print("seabright scalar length: ", len(seabright_scalar_arr))
    print("seabright scaler content: ", seabright_scalar_arr)
    seabright_scalar_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_PIXEL_VALS)

    # read in vector seabright file into array
    seabright_vector_file = open(SEABRIGHT_VECTOR_FILE_NAME, "rb")
    seabright_vector_arr = np.fromfile(SEABRIGHT_VECTOR_FILE_NAME, dtype=int_)
    seabright_vector_file.close()

    # reshape array to 3D vector volume
    print("seabright vector length: ", len(seabright_vector_arr))
    print("seabright vector content: ", seabright_vector_arr)
    seabright_vector_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_VEC_VALS)

    # generate seabright timeline
    generate_seabright_timeline(seabright_scalar_arr, seabright_vector_arr)


    # read in scalar manresa file into array
    manresa_scalar_file = open(MANRESA_SCALAR_FILE_NAME, "rb")
    manresa_scalar_arr = np.fromfile(MANRESA_SCALAR_FILE_NAME, dtype=ubyte)
    manresa_scalar_file.close()

    # reshape array to 3D scalar volume
    print("manresa scalar length: ", len(manresa_scalar_arr))
    print("manresa scalar content: ", manresa_scalar_arr)
    manresa_scalar_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_PIXEL_VALS)

    vector_file_size = 1887436800
    # read in vector manresa file into array
    manresa_vector_file = open(MANRESA_VECTOR_FILE_NAME, "rb")
    manresa_vector_arr = np.fromfile(MANRESA_VECTOR_FILE_NAME,
                                     dtype=int_,
                                     count=int(vector_file_size/2),
                                     sep="",
                                     offset=0)
    manresa_vector_file.close()

    # reshape array to 3D vector volume
    print("manresa vector length: ", len(manresa_vector_arr))
    print("manresa vector content: ", manresa_vector_arr)
    manresa_vector_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_VEC_VALS)

    # generate manresa timeline
    generate_manresa_timeline(manresa_scalar_arr, manresa_vector_arr)

main()
