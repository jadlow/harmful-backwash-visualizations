# Harmful Backwash Visualizations
Download source code from the Harmful Backwash Visualizations GitLab repository and the test data 
from the provided google drive folder into any local directory. Both the timestack and timeline Python
scripts use the Numpy, Pillow, and Open CV libraries which can be installed via pip. The provided code
references the files by name and is only functional for the Manresa and Seabright data sets.
Although the source code is specific to the provided examples, the timestack and timeline methods 
discussed throughout hte paper can be used too visualize any coastal video processed into scalar and vector 
binary files.

After the scripts have been executed, the following visualizations will be saved in the local directory:

seabright_stack.png, manresa_stack.png, seabright_timeline.mp4, manresa_timeline.mp4

The Extended Data (2048) directory contains python scripts compatible with a larger version of the seabright data. In order
to run these scripts, download the source code into a separate local directory and the larger test data from the following google drive:
https://drive.google.com/drive/folders/1k3Q8pbrc_WO1sV21qSVAJ-N0Zbsyaxdu?usp=sharing

After the scripts have been executed, the following visualization will be saved in the local directory:

0seabright_timeline_long.mp4, 141557600seabright_timeline_long.mp4, 2831155200seabright_timeline_long.mp4, 4246732800seabright_timeline_long.mp4, seabright_stack_long.png


Code documentation is integrated in the python script files as comments.
In order to generate the visualizations without error, the Python scripts must be run a computer with at least 16gb of RAM.
