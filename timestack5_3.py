import numpy as np
from numpy import ubyte
from PIL import Image
import cv2

# manresa_scalar_512
# data information
X_RES = 1280
Y_RES = 720
NUM_PIXEL_VALS = 3
NUM_FRAMES = 512
FILE_NAME = "manresa_scalar_512.raw"
FILE_NAME2 = "seabright_scalar.raw"

# generate timestack from scalar 3D volume
def generate_manresa_timestack(arr):
    # define slice line
    line_start = (0, 200)
    line_end = (1279, 719)
    slice_thickness = 1

    # construct first frame of scalar 3D volume into img_arr
    img_arr = np.empty((Y_RES, X_RES, NUM_PIXEL_VALS), dtype=ubyte)
    for col in range(X_RES):
        for row in range(Y_RES):
            for val in range(NUM_PIXEL_VALS):
                img_arr[row, col, 2-val] = arr[0, row, col, val]
    img = cv2.line(img_arr, line_start, line_end, (255, 0, 0), slice_thickness)

    # Save first frame with slice line
    # img_slice = Image.fromarray(img, 'RGB')
    # img_slice.save("manresa_slice", format="PNG")

    # Create list of pixel locations along slice
    coords = []
    for col in range(X_RES):
        for row in range(Y_RES):
            if img[row, col, 0] == 255 and img[row, col, 1] == 0 and img[row, col, 2] == 0:
                coords.append((col, row))

    # initialize angled array
    manresa_timstack_arr = np.empty((len(coords), NUM_FRAMES, NUM_PIXEL_VALS), dtype=ubyte)
    # fill angled timestack using pixel locations along slice
    for frame in range(NUM_FRAMES):
        for row in range(len(coords)):
            for val in range(NUM_PIXEL_VALS):
                manresa_timstack_arr[row, frame, 2-val] = arr[frame, coords[row][1], coords[row][0], val]
    # Manually draw green swash zone boundaries
    # manresa_timstack_arr = cv2.line(manresa_timstack_arr, (0, 400), (511, 400), (0, 255, 0), slice_thickness)
    # manresa_timstack_arr = cv2.line(manresa_timstack_arr, (0, 1060), (511, 1060), (0, 255, 0), slice_thickness)

    # convert 3D scalar volume into png
    img = Image.fromarray(manresa_timstack_arr, 'RGB')
    img.save("manresa_stack", format="PNG")
    img.show()


def generate_seabright_timestack(arr):
    # define slice line
    slice_loc = 639
    line_start = (slice_loc, 0)
    line_end = (slice_loc, 719)
    slice_thickness = 1

    # construct first frame from 3D scalar volume 
    img_arr = np.empty((Y_RES, X_RES, NUM_PIXEL_VALS), dtype=ubyte)
    for col in range(X_RES):
        for row in range(Y_RES):
            for val in range(NUM_PIXEL_VALS):
                img_arr[row, col, 2 - val] = arr[0, row, col, val]
    img = cv2.line(img_arr, line_start, line_end, (255, 0, 0), slice_thickness)

    # save first frame with slice line
    # img_slice = Image.fromarray(img, 'RGB')
    # img_slice.save("seabright_slice", format="PNG")

    # initialized timestack array
    seabright_timestack_arr = np.empty((Y_RES, NUM_FRAMES, NUM_PIXEL_VALS), dtype=ubyte)

    # fill timestack array using pixel locations along slice
    for frame in range(NUM_FRAMES):
        for row in range(Y_RES):
            for val in range(NUM_PIXEL_VALS):
                seabright_timestack_arr[row, frame, 2-val] = arr[frame, row, slice_loc, val]

    # manually draw green swash zone boundaries
    # seabright_timestack_arr = cv2.line(seabright_timestack_arr, (0, 275), (511, 275), (0, 255, 0), slice_thickness)
    # seabright_timestack_arr = cv2.line(seabright_timestack_arr, (0, 535), (511, 535), (0, 255, 0), slice_thickness)

    # convert 3D scalar volume into png
    stack = Image.fromarray(seabright_timestack_arr, 'RGB')
    stack.save("seabright_stack", format="PNG")
    stack.show()


def main():
    # open manresa file and load into 1D array
    manresa_file = open(FILE_NAME, "rb")
    manresa_arr = np.fromfile(FILE_NAME, dtype=ubyte)
    manresa_file.close()
    print("total unsigned chars: ", len(manresa_arr))
    print("total loop iterations: ", NUM_PIXEL_VALS * X_RES * Y_RES * NUM_FRAMES)
    print("total RGB values: ", len(manresa_arr) // NUM_PIXEL_VALS)
    print("total frames: ", (len(manresa_arr) // NUM_PIXEL_VALS) // (X_RES * Y_RES))
    # reshape 1D array into 3D scalar volume
    manresa_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_PIXEL_VALS)
    # generate timestack from 3D scalar volume
    generate_manresa_timestack(manresa_arr)

    # open manresa file and laod into 1D array
    seabright_file = open(FILE_NAME2, "rb")
    seabright_arr = np.fromfile(FILE_NAME2, dtype=ubyte)
    seabright_file.close()
    # reshape into 3D scalar volume
    seabright_arr.shape = (NUM_FRAMES, Y_RES, X_RES, NUM_PIXEL_VALS)
    # generate timestack from 3D scalar volume
    generate_seabright_timestack(seabright_arr)


main()
